// Importar módulo de MongoDB => mongoose
const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    id: Number,
    nombre: String,
    cantidad: Number,
    categoria: String,
    precio: Number
},
{
    versionKey: false,
    timestamps: true
});

module.exports = mongoose.model('productos', productoSchema);