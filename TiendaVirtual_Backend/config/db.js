// Importar módulo de MongoDB => mongoose
const mongoose = require('mongoose');

// Importar la variable de entorno
require('dotenv').config({path: 'var.env'})

const conexionDB = async () => {

    try {
        
        await mongoose.connect(process.env.URL_MONGODB, {});

        console.log("Conexión Establecida con MongoDB Atlas desde archivo db.js");

    } catch (error) {
        console.log("Error de conexión a la base de datos.");
        console.log(error);
        process.exit(1);
    }
}

// exportar como módulo para utilizar en otros archivos
module.exports = conexionDB;