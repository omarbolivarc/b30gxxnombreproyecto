// Forma convencional de ES6 => estándar convencional de JS
// import express from express
// Forma sitema nativo de NodeJS
const express = require('express');

const router = express.Router();

// // Importar módulo de MongoDB => mongoose
// const mongoose = require('mongoose');

// Importar la variable de entorno
// require('dotenv').config({path: 'var.env'})

// importar módulo a la base de datos (archivo db.js)
const conectarDB = require('./config/db');

let app = express();

// app.use('/', function(req, res){
//     res.send("Hola Tripulantes");
// });

app.use(router);


// Conexión a la base de datos
conectarDB();
// console.log(process.env.URL_MONGODB);
// mongoose.connect(process.env.URL_MONGODB)
//     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
//     .catch(function(e){console.log(e)})


// // modelo esquema (schema) de la base de datos => es la estructura...
// const productoSchema = new mongoose.Schema({
//     id: Number,
//     nombre: String,
//     cantidad: Number,
//     categoria: String,
//     precio: Number
// });

// // modelo del producto => 
// // tener en cuenta el nombre de la colección y el esquema 
// const modeloProducto = mongoose.model('productos', productoSchema);

// // CRUD => Create
// modeloProducto.create(
//     {
//         id: 7,
//         nombre: "Punta de anca",
//         cantidad: 123,
//         categoria: "Carnes",
//         precio: 20000
//     },
//     (error) => {
//         // console.log("Ingresó función error...");
//         if (error) return console.log(error);
//         // console.log(error);
//         // console.log("Sale de la función error...");
//     });


// // CRUD => Read
// modeloProducto.find((error, productos) => {
//     if (error) return console.log(error);
//     console.log(productos);
// });


// // CRUD => Update
// modeloProducto.updateOne({id: 7}, {cantidad: 321}, (error) => {
//     if (error) return console.log(error);
// });


// // CRUD => Delete
// modeloProducto.deleteOne({id: 7}, (error) => {
//     if (error) return console.log(error);
// });




// ------------------------------------------------
// #######   Descentralización del CRUD  ##########
// #######      Rutas Respecto al CRUD   ##########
// ------------------------------------------------

// uso de archivos tipo json en la app
app.use(express.json());
// CORS => mecanismos o reglas de seguridad para el contrel de peticiones http
const cors = require('cors');

// solicitudes al CRUD => el controlador
const crudProductos = require('./controllers/controlProducts');

// Establecer las rutas respecto al CRUD
// CRUD => Create
router.post('/', crudProductos.crear);
// CRUD => Read
router.get('/', crudProductos.obtener);
// CRUD => Update
router.put('/:id', crudProductos.actualizar);
// CRUD => Delete
router.delete('/:id', crudProductos.eliminar);





// router.get('/metodoget', function(req, res){
//     res.send("Utilizando el método GET...")
// });
router.get('/metodoget', (req, res) => {
    res.send("Utilizando el método GET...")
    // conectarDB();
});

router.post('/metodoget', function(req, res){
    res.send("Utilizando el método POST... desde la ruta /metodoget")
});

router.post('/postmetodo', function(req, res){
    res.send("Ruta /postmetodo... Utilizando el método POST...")

    // Conexión con la base de datos
    // const user = 'b30gxx';
    // const psw = 'b30gxx123';
    // const db = 'b30gxx';
    // const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

    // mongoose.connect(url)
    //     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
    //     .catch(function(e){console.log(e)})
});



// // Conexión con la base de datos
// const user = 'b30gxx';
// const psw = 'b30gxx123';
// const db = 'b30gxx';
// const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

// mongoose.connect(url)
//     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
//     .catch(function(e){console.log(e)})



app.listen(4000);

console.log("La aplición se ejecuta en http://localhost:4000");